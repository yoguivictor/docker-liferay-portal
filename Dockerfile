# Liferay Portal 6.2 EE Compatible Environment
# https://www.liferay.com/documents/14/21598941/Liferay+Portal+6.2+EE+Compatibility+Matrix.pdf

# CentOS 6 (6-6.el6.centos.12.2.x86_64)
FROM		centos:6

# Oracle JDK 7 (7u80-b15)
RUN		yum install -y wget tar \
	&&	wget 	--no-check-certificate --no-cookies --header "Cookie: oraclelicense=accept-securebackup-cookie" \
			http://download.oracle.com/otn-pub/java/jdk/7u80-b15/jre-7u80-linux-x64.rpm \
	&&	rpm -Uvh jre-7u80-linux-x64.rpm \
	&&	rm -f jre-7u80-linux-x64.rpm

ENV		LIFERAY_HOME				/opt/lportal
ENV		TOMCAT_HOME				${LIFERAY_HOME}/tomcat-7.0.42
ENV		LIFERAY_WEB_SERVER_HOST			localhost
ENV		LIFERAY_WEB_SERVER_HTTP_PORT		8080
ENV		LIFERAY_WEB_SERVER_HTTPS_PORT		8443
ENV		LIFERAY_WEB_SERVER_PROTOCOL		http
ENV		LIFERAY_JDBC_DEFAULT_USERNAME		lportal
ENV		LIFERAY_JDBC_DEFAULT_PASSWORD		lportal
ENV		LIFERAY_JDBC_DEFAULT_DRIVERCLASSNAME	com.mysql.jdbc.Driver
ENV		LIFERAY_JDBC_DEFAULT_URL		jdbc:mysql://db/lportal?useUnicode=true&characterEncoding=UTF-8&useFastDateParsing=false
ENV		LIFERAY_ADMIN_EMAIL_FROM_ADDRESS	test@liferay.com
ENV		LIFERAY_ADMIN_FROM_NAME			Test Test
ENV		LIFERAY_SETUP_WIZARD_ENABLED		false
ENV		JAVA_OPTS				-Xms1024m -Xmx1024m -XX:MaxPermSize=256m

# Liferay Portal 6.2 EE Tomcat Bundle (sp11) 30-day-trial from liferay.com
ADD		liferay-portal-6.2-ee-sp11/		$LIFERAY_HOME/

# MySQL Java Connector (5.1.35)
RUN		wget http://dev.mysql.com/get/Downloads/Connector-J/mysql-connector-java-5.1.35.tar.gz -qO- | tar xz -C /tmp \
	&&	mv /tmp/mysql-connector-java-5.1.35/mysql-connector-java-5.1.35-bin.jar ${TOMCAT_HOME}/lib/ext/ \
	&&	rm -rf /tmp/*

ADD		deploy					$LIFERAY_HOME/deploy/
ADD		bin					/usr/local/bin/

CMD		["tomcat"]

